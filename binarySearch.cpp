#include <iostream>
using namespace std;

int binarySearch(int *,int ,int,int);
static int i=0;
int main()
{
    int size=5,val;
    int array[size];
    cout<<"Enter the elements in sorted order: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    cout<<"Enter the value to search: ";
    cin>>val;
    int pos=binarySearch(array,0,size,val);
    if (pos!=-1)
        cout<<"Value Found! Position= "<<pos<<endl;
    else
        cout<<"Value not found!"<<endl;
    return 0;
}


int binarySearch(int * a,int beg ,int end,int val)
{
    if (beg<=end)
    {
        ++i;
        cout<<i<<"th pass: "<<endl;
        int med=(int) (beg+end)/2;
        cout<<"Middle value= "<<a[med]<<"\t Given value="<<val<<endl;
        if( a[med]==val)
        {
            cout<<"Matched!"<<endl;
            return med;
        }
        else if (a[med]<val)
        {
            cout<<"Not matched!"<<endl;
            return binarySearch(a,med+1,end,val);
        }
        else 
        {
            cout<<"Not matched!"<<endl;
            return binarySearch(a,beg,med-1,val);
        }
    }
    return -1;


}





