#include<iostream>
using namespace std;

void bubbleSort(int *a,int n);
void swap(int &,int &);

int main()
{
    int size;
    cout<<"Enter the size of the array: ";
    cin>>size;
    int array[size];
    cout<<"Enter the elements: ";
    for(int i=0;i<size;i++)
        cin>>array[i];
    bubbleSort(array,size);
    cout<<"The sorted array: ";
    for(int i=0;i<size;i++)
        cout<<array[i]<<" ";
    cout<<endl;
    return 0;
}

void bubbleSort(int *a,int n)
{
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<n-i;j++)
            if (a[j]>a[j+1])
                swap(a[j],a[j+1]);
        cout<<i+1<<"th pass: ";

        for(int j=0;j<n;j++)
            cout<<a[j]<<" ";
        cout<<endl;
    }
}


void swap(int &a,int &b)
{
    int temp=b;
    b=a;
    a=temp;
}









